//
//  MEditingViewController.h
//  mymusic
//
//  Created by Dalia on 2/22/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMasterViewController;

@protocol UIFavProtocol
//- (BOOL) choice;
@end

@interface MEditingViewController : UIViewController
{
    id <UIFavProtocol> delegate;
    __weak IBOutlet UISwitch *mySwitch;
}
@property (retain, nonatomic) id <UIFavProtocol> delegate;
@property (nonatomic, retain) IBOutlet UISwitch *mySwitch;
- (IBAction)changeFav:(id)sender;

@end
