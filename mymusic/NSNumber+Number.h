//
//  NSNumber+Number.h
//  mymusic
//
//  Created by Dalia on 2/16/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Number)

- (int) genreIdentifier;

@end
 