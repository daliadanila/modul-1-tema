//
//  MMasterViewController.m
//  mymusic
//
//  Created by Dalia on 2/9/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import </usr/include/objc/runtime.h>
#import "MMasterViewController.h"
#import "MDetailViewController.h"
#import "NSNumber+Number.h"
#import "Tracks_ExtensionTracks.h"

static NSString *CellIdentifier = @"Cell";


@implementation MMasterViewController
@synthesize _objects;
//@synthesize row;
//@synthesize section;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.title = NSLocalizedString(@"Playlist", @"Playlist");
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            //am comm dupa ce am schimbat clasa parinte pt masterview
            //self.clearsSelectionOnViewWillAppear = NO;
            self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
        }
    }
    return self;
}

- (id)mMasterViewController
{
        
    //NSLog(@"self************* %@", _objects);
    return _objects;
}

- (void)viewDidLoad
{
	// Do any additional setup after loading the view, typically from a nib.
    NSString *errorDesc = nil;
    NSString *error;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:@"tracks.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        NSString *alta = [[NSBundle mainBundle] pathForResource:@"tracks" ofType:@"plist"];
        [[NSFileManager defaultManager] copyItemAtPath:alta toPath:plistPath error:&error];
    }
    
    NSLog(@"plistPath %@",plistPath);
    NSLog(@"Home Direct %@",NSHomeDirectory());
    NSLog(@"rootPath %@", rootPath);
    // citesc din plist toate datele
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    
    _objects = (NSMutableArray *)[NSPropertyListSerialization
                                  propertyListFromData:plistXML
                                  mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                  format:&format
                                  errorDescription:&errorDesc];
    
    
    if (!_objects) {
        NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
    }
    tracks = [[NSMutableArray alloc] init];
    
    // in tracks am creat vectorul de instante
    Tracks *t=[[Tracks alloc]init];
    for(int i=0;i<_objects.count;i++)
    {
        t = [[Tracks alloc] init:_objects :i];
        [tracks addObject:t];
        NSLog(@"-----\r");
        NSLog(t.description);
    }
    
    // in genreA am creat vectorul de vectori pentru gen
    genreA = [NSMutableArray new];
    genreA = genreArrayBlock(tracks);
    /* scriere in plist
     
     NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
     for(int i=0;i<_objects.count;i++)
     {
     d=(NSMutableDictionary *)[_objects objectAtIndex:i];
     [d setObject:[NSNumber numberWithInt:1] forKey:@"genre"];
     
     }
     
     
     NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:_objects
     format:NSPropertyListXMLFormat_v1_0
     errorDescription:&error];
     if(plistData) {
     [plistData writeToFile:plistPath atomically:YES];
     }
     else {
     NSLog(error);
     }*/
    
      NSLog(@"method name: %@", NSStringFromSelector(_cmd));  
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
   
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    
    //am comm dupa ce am schimbat clasa parinte pt masterview
    //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //[self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionn
{
    switch (sectionn) {
        case 0: return 2;
        case 1: return 2;
        case 2: return 2;
        case 3: return 1;
        case 4: return 0;
        case 5: return 1;
        default: return 1;
    }
}

// block care creaza vectorul de vectori pt gen
NSMutableArray* (^genreArrayBlock)(NSMutableArray*)=^(NSMutableArray* tracks)
{
    
    genree gen = 0;
    NSMutableArray *gArray = [[NSMutableArray alloc]initWithCapacity:5];
    
    Tracks *t = [Tracks new];
    
    while(gen!=6)
    {
        NSMutableArray *ga = [NSMutableArray new];
        for(int i=0;i<8;i++)
       {
           t = (NSMutableArray*)[tracks objectAtIndex:i];
          // t = (NSMutableArray *)t;
           //     NSLog(@"t inainte %@",t);
           if(t.gen == gen)
        {
            [ga addObject:t];
            //NSLog(@"t dupa %@",t);
        }           
       }
      //  NSLog(@">>>>ga %@",ga);
        [gArray addObject:ga];
        gen++;
    }
  
   // NSLog(@"gArray:object ---- %@",[gArray objectAtIndex:0]);
   // NSLog(@"dimensiune gArray %d",gArray.count);
    
    return gArray;
};

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSMutableArray *tableContent = genreArrayBlock(tracks);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

   if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    
    Tracks *t1 = [[genreA objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

    cell.textLabel.text =  t1.titlu;
    cell.detailTextLabel.text = t1.artist;
           

    NSLog(@"method name: %@", NSStringFromSelector(_cmd));
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // cu vectorul de instante Track am facut legatura pt a afisa detalii coresp fiecarei linii din master
    //Tracks *object = tracks[indexPath.row];
   // NSDate * object = _objects[indexPath.row];

    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    if (!self.detailViewController) {
	        self.detailViewController = [[MDetailViewController alloc] initWithNibName:@"MDetailViewController_iPhone" bundle:nil];
	    }
	    self.detailViewController.detailItem = [[genreA objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
      
        if([self respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)])
            NSLog(@"tableView:cellRowAtIndexPath: implemented");
        if([self respondsToSelector:@selector(tableView:numberOfRowsInSection:)])
            NSLog(@"tableView:numberOfRowsInSection: implemented");
            
        [self.navigationController pushViewController:self.detailViewController animated:YES];
        
       // objc_setAssociatedObject(self, (__bridge const void *)(CellIdentifier), genreA, OBJC_ASSOCIATION_ASSIGN);
        //objc_setAssociatedObject(self, (__bridge const void *)(CellIdentifier), [[genreA objectAtIndex:indexPath.section] objectAtIndex:indexPath.row], OBJC_ASSOCIATION_ASSIGN);
    } else {
        self.detailViewController.detailItem = [[genreA objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    
    
   // row=objc_getAssociatedObject(self, (__bridge const void *)(CellIdentifier));
  //  NSLog(@"rowwwwww %@",row);

}


// punele numele sectiunilor in functie de gen
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(genree)sectionn
{
    switch (sectionn) {
        case Alternative:
            return @"Alternative";
        case Blues:
            return @"Blues";
        case Country:
            return @"Country";
        case HipHop:
            return @"Hip Hop";
        case Indie:
            return @"Indie";
        case Pop:
            return @"Pop";
    }
}


@end

@implementation Tracks

@synthesize artist;
@synthesize titlu;
@synthesize gen;
@synthesize durata;
@synthesize favorit;

- (id)init:(NSMutableArray *)temp :(int) i
{
        
         self = [super init];
         if (self) {
        
             NSDictionary *dict=(NSDictionary*)[temp objectAtIndex:i];
             self.titlu = [dict objectForKey:@"title"];
             self.artist = [dict objectForKey:@"artist"];
             self.gen = [[dict objectForKey:@"genre"]genreIdentifier];
             self.durata = [dict objectForKey:@"duration"];
             self.favorit = [[dict objectForKey:@"favorited"] boolValue];
            
            
        }
        return self;
}
+ (Tracks *)tracks
{
    
    return self.tracks;
}

// am suprapus metoda description
- (NSString *) description
{
    NSString * str=[NSString new];
    switch(self.gen)
    {
        case Alternative: str = @"Alternative"; break;
        case Blues: str = @"Blues"; break;
        case Country: str = @"Country"; break;
        case Indie: str = @"Indie"; break;
        case HipHop: str = @"Hip Hop"; break;
        case Pop: str = @"Pop"; break;
    }
    NSString *s = [NSString stringWithFormat:@"Title: %@ \r Artist: %@ \r Genre: %@ \r Duration: %@ \r Favourited: %d ",self.titlu,self.artist, str,self.durata,self.favorit];

    return s;
}

- (NSString *)pentruArtist:(NSMutableArray*)tracks index:(int)i
{
    Tracks *t=[tracks objectAtIndex:i];
    return t.artist;
}

-(NSString *)pentruTitlu:(NSMutableArray*)tracks index:(int)i
{
    Tracks *t=[tracks objectAtIndex:i];
    return t.artist;
}

-(genree)pentruGen:(NSMutableArray*)tracks index:(int)i
{
    Tracks *t=[tracks objectAtIndex:i];
    return t.gen;
}

-(NSNumber *)pentruDurata:(NSMutableArray*)tracks index:(int)i
{
    Tracks *t=[tracks objectAtIndex:i];
    return t.durata;
}

-(BOOL)pentruFavorit:(NSMutableArray*)tracks index:(int)i
{
    Tracks *t=[tracks objectAtIndex:i];
    return t.favorit;
}

@end

