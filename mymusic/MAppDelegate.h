//
//  MAppDelegate.h
//  mymusic
//
//  Created by Dalia on 2/9/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) UISplitViewController *splitViewController;

@end 
