//
//  MDetailViewController.m
//  mymusic
//
//  Created by Dalia on 2/9/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "MDetailViewController.h"
#import "MMasterViewController.h"
#import "MEditingViewController.h"

@class MMasterViewController;

@interface MDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (IBAction)printFirstName:(UIButton *)sender;

@property(getter=getNume, setter = setNume:)CFStringRef nume;
- (void)setNume:(CFStringRef) n;
- (CFStringRef)getNume;

- (void)configureView;
@end

@implementation MDetailViewController

#pragma mark - Managing the detail item

- (void)setNume:(CFStringRef) n
{
    //CFStringRef temp;// = self.nume;
    self.nume = CFStringCreateCopy(NULL, n);
   // CFRelease(temp);
    
}

- (CFStringRef)getNume
{
    return self.nume;
}

// afisare string cu numele meu fol o var de tipul CFStringRef
- (IBAction)printFirstName:(UIButton *)sender
{
    CFStringRef n;
    NSString *myName = @"Dalia";
    n = (__bridge CFStringRef)myName;
    [self setNume:n];
    //firstName = n;
   
    n = [self getNume];
    myName = (__bridge NSString *)n;
    
    //CFRelease(n);
     
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"My name is" message:@"Dalia" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    // optional - add more buttons:
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
    
    NSLog(@"method name: %@", NSStringFromSelector(_cmd));
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
    NSLog(@"method name: %@", NSStringFromSelector(_cmd));
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    
    NSLog(@"method name: %@", NSStringFromSelector(_cmd));
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    NSLog(@"method name: %@", NSStringFromSelector(_cmd));}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Detail", @"Detail");
    }
    
    NSLog(@"method name: %@", NSStringFromSelector(_cmd));
    return self;
}
							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
    
    NSLog(@"method name: %@", NSStringFromSelector(_cmd));
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
    
    NSLog(@"method name: %@", NSStringFromSelector(_cmd));
}


- (IBAction)onEditTouch:(id)sender {
    
    MEditingViewController *editingViewController = [[MEditingViewController alloc] initWithNibName:@"MEditingViewController" bundle:nil];
    
    [self.navigationController pushViewController:editingViewController animated:YES];
}
@end
