//
//  NSNumber+Number.m
//  mymusic
//
//  Created by Dalia on 2/16/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import "NSNumber+Number.h"

@implementation NSNumber (Number)
- (int) genreIdentifier
{
    return [self intValue];
}

@end
 