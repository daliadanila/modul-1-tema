//
//  MMasterViewController.h
//  mymusic
//
//  Created by Dalia on 2/9/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MDetailViewController;

@interface MMasterViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *_objects; //vectorul care rezulta in urma citirii plistului
    NSMutableArray *tracks; // vector de instante Track
    NSMutableArray *genreA; // vector de vectori sortat dupa gen
    
}

@property (strong,nonatomic) NSMutableArray *_objects;
@property (strong, nonatomic) MDetailViewController *detailViewController;
//@property NSNumber* row;
//@property NSNumber* section;

//@property(strong,nonatomic) NSMutableArray* tracks;@end
typedef enum{ Alternative = 0, Blues = 1, Country = 2, HipHop = 3, Indie = 4, Pop = 5 }genree;
- (id)mMasterViewController;
@end

@interface Tracks : NSObject
{
    NSString *titlu;
    NSString *artist;
    genree gen;
    NSNumber *durata;
    Boolean favorit;
     
}
@property (strong, nonatomic) NSString *titlu;
@property (strong, nonatomic) NSString *artist;
@property (nonatomic) genree gen;
@property (readonly) NSNumber *durata;
@property (nonatomic) Boolean favorit;

- (id)init:(NSMutableArray *)temp: (int) i;
- (NSString *) description;
+ (Tracks *)tracks;
- (NSString *)pentruArtist:(NSMutableArray*)tracks index:(int)i;
-(NSString *)pentruTitlu:(NSMutableArray*)tracks index:(int)i;
-(genree)pentruGen:(NSMutableArray*)tracks index:(int)i;
-(NSNumber *)pentruDurata:(NSMutableArray*)tracks index:(int)i;
-(BOOL)pentruFavorit:(NSMutableArray*)tracks index:(int)i;
@end
