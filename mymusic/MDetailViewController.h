//
//  MDetailViewController.h
//  mymusic
//
//  Created by Dalia on 2/9/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEditingViewController.h"

@class Tracks;

@interface MDetailViewController : UIViewController <UISplitViewControllerDelegate, UIFavProtocol>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
- (IBAction)onEditTouch:(id)sender;

@end
 